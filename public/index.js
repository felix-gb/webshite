System.register("foo", [], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    function hello() {
        console.log('heelo');
    }
    exports_1("hello", hello);
    return {
        setters: [],
        execute: function () {
        }
    };
});
System.register("index", ["foo"], function (exports_2, context_2) {
    "use strict";
    var foo_1;
    var __moduleName = context_2 && context_2.id;
    return {
        setters: [
            function (foo_1_1) {
                foo_1 = foo_1_1;
            }
        ],
        execute: function () {
            foo_1.hello();
        }
    };
});
//# sourceMappingURL=index.js.map