const magnitude = 12;
const numCells = Math.pow(2, magnitude);

export type Grid = Array<Array<number>>;

interface Point {
  x: number;
  y: number;
}

function modulo(a: number, b: number): number {
  return ((a % b) + b) % b;
}

function findMidpoint(tl: Point, br: Point): Point {
  return { 
    x: (tl.x + br.x) / 2,
    y: (tl.y + br.y) / 2
  };
}

function diamondPoints(center: Point, size: number): Array<Point> {
  const r = size / 2;
  return [
    { x: center.x - r, y: center.y },
    { x: center.x, y: center.y - r },
    { x: center.x + r, y: center.y },
    { x: center.x, y: center.y + r }
  ];
}

export class DiamondSquare {

  step = 1;
  readonly grid: Grid = [];

  constructor() {
    this.initGrid();
  }

  average(points: Array<Point>): number {
    const sum = points
      .map(p => this.grid[modulo(p.x, numCells + 1)][modulo(p.y, numCells + 1)])
      .reduce((a, v) => a + v, 0);

    return sum / points.length;
  }

  generate(): Grid {
    this.go();
    return this.grid;
  }

  initGrid(): void {
    for (let i = 0; i <= numCells; i++) {
      this.grid[i] = [];
      for (let j = 0; j <= numCells; j++) {
        this.grid[i][j] = 0;
      }
    }
    this.grid[0][0] = Math.random() * 100;
    this.grid[0][numCells] = Math.random() * 200;
    this.grid[numCells][numCells] = Math.random() * 2000;
    this.grid[0][numCells] = Math.random() * 4000;
  }

  iterGrid(): void {
    const numCellsNow = Math.pow(2, this.step - 1);
    const squareSize = numCells / numCellsNow

    if (squareSize <= 1) return;

    const stepRand = () => ((Math.random() - 0.5) * 400) / this.step

    for (let i = 0; i < numCellsNow; i++) {
      for (let j = 0; j < numCellsNow; j++) {
        const tl = { 
          x: i * squareSize,
          y: j * squareSize
        }
        const br = { 
          x: (i * squareSize) + squareSize,
          y: (j * squareSize) + squareSize
        }
        const tr = { x: br.x, y: tl.y };
        const bl = { x: tl.x, y: br.y };
        const squarepoints = [ tl, tr, br, bl ];
        const midpoint = findMidpoint(tl, br)
        const newSquareValue = this.average(squarepoints) + stepRand();
        this.grid[midpoint.x][midpoint.y] = newSquareValue;

        for (let k = 0; k < 4; k++) {
          const diamondMidpoint = findMidpoint(squarepoints[k], squarepoints[(k + 1) % 4]);
          const points = diamondPoints(diamondMidpoint, squareSize);
          const newDiamondValue = this.average(points) + stepRand();
          this.grid[diamondMidpoint.x][diamondMidpoint.y] = newDiamondValue;
        }
      }
    }

    this.step++;
  }

  go(): void {
    for (let i = 0; i < magnitude; i++) {
      this.iterGrid();
    }
  }

}

export function go(): void {
  // drawGrid();
  const cd = new DiamondSquare();
  cd.go();
  // animate();
}
