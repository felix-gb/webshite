import { DiamondSquare, Grid } from './diamondSquare';

export const fragmentShader = `
precision mediump float;

uniform sampler2D u_texture;

varying vec2 v_texcoord;

uniform vec2 u_imgsize;
uniform float t_now;

void main() {
  vec2 p = (-u_imgsize.xy + 2.0 * gl_FragCoord.xy) / u_imgsize.y;

  float a = atan(p.y, p.x);
  float r = length(p);

  vec2 uv = vec2(0.3 / r + t_now, a / 3.1415927);
  vec3 col = texture2D(u_texture, uv).xyz;

  col = col * r;

  gl_FragColor = vec4(col, 1.0);
}
`

export const directFragmentShader = `
precision mediump float;

uniform sampler2D u_texture;
uniform float t_now;

varying vec2 v_texcoord;

void main() {
  vec3 col = texture2D(u_texture, v_texcoord).xyz;
  col = col * t_now;
  gl_FragColor = texture2D(u_texture, v_texcoord);
}
`

export const swirlsFragShader = `
precision mediump float;

uniform sampler2D u_texture;

varying vec2 v_texcoord;

uniform vec2 u_imgsize;
uniform float t_now;

vec3 deform(vec2 p, float t) {
  p += 0.5 * sin(t * vec2(1.1, 1.3) + vec2(0.0, 0.5));

  float a = atan(p.y, p.x);
  float r = length(p);

  float s = r * (1.0 + 0.5 * cos(t * 1.7));

  vec2 uv = 0.1 * t + 0.05 * p.yx + 0.05 * vec2(cos(t + a * 2.0), sin(t + a * 2.0)) / s;

  return texture2D(u_texture, 0.5 * uv).xyz;
}

void main() {
  vec2 q = gl_FragCoord.xy / u_imgsize.xy;
  vec2 p = -1.0 + 2.0 * q;

  vec3 col = vec3(0.0);
  for (int i = 0; i < 15; i++) {
    float t = t_now + float(i) * 0.007;
    col += deform(p, t);
  }

  col /= 20.0;
  col = col.xyz;
  col *= 1.5 * (0.5 + 0.5 * pow(16.0 * q.x * q.y * (1.0 - q.x) * (1.0 - q.y), 0.25));
  gl_FragColor = vec4(col, 1.0).rgba;
}
`

const vertexShader = `
attribute vec2 a_position;
attribute vec2 a_texcoord;

uniform vec2 u_resolution;

varying vec2 v_texcoord;

void main() {
  vec2 zeroToOne = a_position / u_resolution;
  vec2 zeroToTwo = zeroToOne * 2.0;
  vec2 clipSpace = zeroToTwo - 1.0;

  gl_Position = vec4(clipSpace * vec2(1, -1), 0, 1);

  v_texcoord = a_texcoord;
}
`

function compileShader(gl: WebGLRenderingContext, shaderSource: string, shaderType: number): WebGLShader {
  const shader = gl.createShader(shaderType)!;

  gl.shaderSource(shader, shaderSource);
  gl.compileShader(shader);

  const success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (!success) {
    throw new Error(`Could not compile shader of type ${shaderType}: ${gl.getShaderInfoLog(shader)}`);
  }

  return shader;
}

function createShaderProgram(gl: WebGLRenderingContext, vertexShader: WebGLShader, fragmentShader: WebGLShader): WebGLProgram {
  const program = gl.createProgram()!;

  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);

  gl.linkProgram(program);

  const success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (!success) {
    throw new Error(`Could not link program: ${gl.getProgramInfoLog(program)}`);
  }

  return program;
}

async function loadImage(src: string): Promise<HTMLImageElement> {
  return new Promise((resolve, reject) => {
    const img = new Image();
    img.onload = () => resolve(img);
    img.onerror = reject;
    img.src = src;
  });
}

export function loadTexture(gl: WebGLRenderingContext, img: HTMLImageElement): WebGLTexture {
  const texture = gl.createTexture();
  if (texture == null) throw new Error('error creating texture');

  gl.bindTexture(gl.TEXTURE_2D, texture);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img);

  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

  return texture;
}

function setRectangle(gl: WebGLRenderingContext, x1: number, y1: number, width: number, height: number): void {
  const x2 = x1 + width;
  const y2 = y1 + height;
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
    x1, y1,
    x2, y1,
    x1, y2,
    x1, y2,
    x2, y1,
    x2, y2
  ]), gl.STATIC_DRAW);
}

function drawImage(gl: WebGLRenderingContext, program: WebGLProgram, image: HTMLImageElement, gridBuffer: Uint8Array): void {
  const positionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  setRectangle(gl, 0, 0, gl.canvas.width, gl.canvas.height);

  const textureCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
    0.0,  0.0,
    1.0,  0.0,
    0.0,  1.0,
    0.0,  1.0,
    1.0,  0.0,
    1.0,  1.0,
  ]), gl.STATIC_DRAW);

  bufferToTexture(gl, gridBuffer);
  // loadTexture(gl, image);

  const loop = (currentTime: number) => {
    requestAnimationFrame(loop);

    gl.useProgram(program);

    const positionLocation = gl.getAttribLocation(program, "a_position");
    const texcoordLocation = gl.getAttribLocation(program, "a_texcoord");

    const timeLocation = gl.getUniformLocation(program, 't_now');

    const resolutionLocation = gl.getUniformLocation(program, "u_resolution");
    const textureSizeLocation = gl.getUniformLocation(program, "u_imgsize");

    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

    gl.clearColor(0, 0, 0, 0);
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.enableVertexAttribArray(positionLocation);

    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

    gl.vertexAttribPointer(
      positionLocation, 2, gl.FLOAT, false, 0, 0
    );

    gl.enableVertexAttribArray(texcoordLocation);
    gl.bindBuffer(gl.ARRAY_BUFFER, textureCoordBuffer);

    gl.vertexAttribPointer(
      texcoordLocation, 2, gl.FLOAT, false, 0, 0
    );

    gl.uniform1f(timeLocation, currentTime / 1000);
    gl.uniform2f(resolutionLocation, gl.canvas.width, gl.canvas.height);
    gl.uniform2f(textureSizeLocation, gl.canvas.width, gl.canvas.height);
    gl.drawArrays(gl.TRIANGLES, 0, 6)
  }

  requestAnimationFrame(loop);
}

function loadCanvas(): HTMLCanvasElement {
  const canvas = <HTMLCanvasElement> document.getElementById('tunnel_canvas')!;
  const resize = () => {
    canvas.width = window.innerWidth
    canvas.height = window.innerHeight
  };

  resize();
  window.addEventListener('resize', resize);

  return canvas;
}

function makeShaderProgram(gl: WebGLRenderingContext): WebGLProgram {
  const vert = compileShader(gl, swirlsFragShader, gl.FRAGMENT_SHADER);
  const frag = compileShader(gl, vertexShader, gl.VERTEX_SHADER);
  return createShaderProgram(gl, vert, frag);
}

export function go(): void {
  const canvas = loadCanvas();
  const gl = canvas.getContext('webgl')!;
  const program = makeShaderProgram(gl);

  const diamondSquareGrid = new DiamondSquare().generate();
  const gridBuffer = gridToBuffer(diamondSquareGrid);

  loadImage('kq.jpg').then(img => {
    drawImage(gl, program, img, gridBuffer);
  }).catch(err => {
    console.error(err);
  });
}

export function gridToBuffer(grid: Grid): Uint8Array {
  const buffer = new Uint8Array(grid.length * grid.length * 4);

  let n = 0;
  for (let i = 0; i < grid.length - 1; i++) {
    for (let j = 0; j < grid.length - 1; j++) {
      buffer[n++] = 0;
      buffer[n++] = Math.round(grid[i][j]);
      buffer[n++] = Math.round(grid[i][j]);
      buffer[n++] = 255;
    }
  }

  return buffer;
}

export function bufferToTexture(gl: WebGLRenderingContext, buffer: Uint8Array): WebGLTexture {
  const texture = gl.createTexture()!;
  gl.bindTexture(gl.TEXTURE_2D, texture);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1024, 1025, 0, gl.RGBA, gl.UNSIGNED_BYTE, buffer);

  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

  return texture;
}
