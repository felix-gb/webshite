import * as T from 'three';

export function flatten(grid: Array<Array<number>>): Array<number> {
  const out = []
  for (let i = 0; i < grid.length - 1; i++) {
    for (let j = 0; j < grid.length - 1; j++) {
      out.push(i)
      out.push(norm(grid[i][j]))
      out.push(j)
      out.push(i + 1)
      out.push(norm(grid[i + 1][j]))
      out.push(j)
      out.push(i + 1)
      out.push(norm(grid[i + 1][j + 1]))
      out.push(j + 1)
    }
  }
  return out;
}

function norm(n: number): number {
  return n / 25;
}

function geometryFromVerts(verts: Array<number>): T.BufferGeometry {
  const geometry = new T.BufferGeometry();
  const vertices = new Float32Array(verts);
  geometry.addAttribute('position', new T.BufferAttribute(vertices, 3));
  return geometry;
}

export class GridDisplay {

  width: number = 0;
  height: number = 0;

  readonly camera = new T.PerspectiveCamera(45, document.body.offsetWidth / document.body.offsetHeight, 1, 1000);
  readonly renderer = new T.WebGLRenderer({ antialias: true });
  readonly scene = new T.Scene();

  constructor(verts: Array<number>) {
    this.resize();
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(this.width, this.height);
    document.body.appendChild(this.renderer.domElement);
    document.body.addEventListener('resize', this.resize);
    this.camera.position.z = -20;
    this.camera.position.y = 50;
    this.camera.position.x = -20;
    this.camera.lookAt(200, 0, 200);
    this.scene.background = new T.Color(0xffffff);


    const geometry = geometryFromVerts(verts);
    const wireframe = new T.WireframeGeometry(geometry);
    const mat = new T.LineBasicMaterial( { color: 0xff0000, linewidth: 2 } );
    const line = new T.LineSegments(wireframe, mat);

    this.scene.add(line);
  }

  loop = () => {
    requestAnimationFrame(this.loop)
    this.renderer.render(this.scene, this.camera);
  }

  resize = () => {
    this.height = document.body.offsetHeight;
    this.width = document.body.offsetWidth;
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix;
  }

}
